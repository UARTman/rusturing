use std::collections::HashSet;
use cursive::Cursive;
use cursive::views::{Dialog, TextView, LinearLayout, Menubar, Button, DummyView};
mod engine;

fn main() {
//    let mut abc = HashSet::new();
//    abc.insert('a');
//    let mut a = engine::Logic::new(1, abc);
//    a.set_instruction(2, 'b', engine::Instruction::new());

    // Creates the cursive root - required for every application.
    let mut siv = Cursive::default();
    siv.set_user_data::<engine::Actor>(engine::Actor { last_print:0, states:5, alphabet:engine::Alphabet::from_str("_12") });
    let mut actor = siv.user_data::<engine::Actor>().unwrap();

    // Creates a dialog with a single "Quit" button
    let mut tape_layout = LinearLayout::horizontal();
    for i in 0..10 {
        tape_layout.add_child(Button::new(i.to_string(), move |s| tape_button_cb(s, i)));
    }

    let mut logic_layout = LinearLayout::horizontal();
    let mut row = LinearLayout::vertical().child(DummyView);
    for i in actor.alphabet.set.iter(){
       row.add_child(TextView::new(i.to_string()));
    }
    logic_layout.add_child(row);
    for i in 0..actor.states {
        let mut row = LinearLayout::vertical();
        row.add_child(TextView::new(i.to_string()));
        for j in actor.alphabet.set.iter() {
            row.add_child(Button::new(j.to_string(), dummy_cb))
        }
        logic_layout.add_child(row)
    }


    let mut root_layout = LinearLayout::vertical()
        .child(tape_layout
            .child(DummyView)
            .child(TextView::new("Panic: "))
        )
        .child(logic_layout);

//    let mut dialog = Dialog::around(TextView::new("Hello Dialog!"))
//        .title("Cursive")
//        .button("Quit", |s| s.quit());
//
//    for i in 0..10 {
//        dialog.add_button(i.to_string(), dummy_cb);
//    }

//    siv.add_layer(dialog);

    siv.add_layer(root_layout);

    // Starts the event loop.
    siv.run();
}

fn dummy_cb(c: &mut Cursive){
}

fn tape_button_cb(c: &mut Cursive, i: u32){
    let last = c.user_data::<engine::Actor>().unwrap().last_print;
    c.add_layer(Dialog::around(TextView::new(last.to_string()))
        .button("Close", |s| {s.pop_layer();}));
    c.user_data::<engine::Actor>().unwrap().last_print = i;
}
