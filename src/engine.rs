use std::collections::{HashSet, HashMap, VecDeque};
use std::borrow::Borrow;

pub enum AMovement{
    Left, Right
}

pub struct Instruction{
    state: Option<u32>,
    character: Option<char>,
    movement: Option<AMovement>,
}

impl Instruction{
    pub fn new() -> Instruction{
        Instruction{state: None, character: None, movement:None }
    }
}

pub struct Logic{
    logic: HashMap<u32, HashMap<char, Instruction>>,
    states: u32,
    chars: HashSet<char>,
}

impl Logic{
    pub fn new(states: u32, chars: HashSet<char>) -> Logic{
        let mut hash = HashMap::new();
        for i in 0..states {
            let mut under_hash = HashMap::new();
            for &j in chars.iter() {
                under_hash.insert(j, Instruction::new());}
            hash.insert(i, under_hash);
        }
        Logic{
            logic: hash,
            states,
            chars
        }
    }

    pub fn get_instruction(&self, state: u32, in_char: char) -> Option<&Instruction>{
        let x = self.logic.get(state.borrow());
        match x{
            Some(a) => a.get(in_char.borrow()),
            None => None
        }
    }

    pub fn set_instruction(&mut self, state: u32, in_char: char, instruction: Instruction){
        if state >= self.states {
            return;
        }
        if !self.logic.contains_key(&state){
            self.logic.insert(state, HashMap::new());
        }
        let mut f = self.logic.get_mut(state.borrow()).unwrap();
        f.insert(in_char, instruction);
    }

    pub fn set_states(&mut self, states: u32){
        if states > self.states{
            for i in self.states+1 .. states{
                self.logic.insert(i, HashMap::new());
            }
        }
        if states < self.states{
            for i in states+1 .. self.states{
                self.logic.remove(i.borrow());
            }
        }
        self.states = states;
    }
}


pub struct Alphabet{
    empty: char,
    pub set: HashSet<char>
}

impl Alphabet{
    pub fn from_str(s: &str) -> Alphabet{
        let mut a = true;
        let mut set = HashSet::new();
        let mut e:char = '_';
        for i in s.chars(){
            if a {
                a = false;
                e = i;
            }
            set.insert(i);
        }
        Alphabet{empty: e, set}
    }
}

pub struct Tape {
    body: VecDeque<char>,
    len: u32
}

impl Tape{
    pub fn new(len: u32, empty: char) -> Tape{
        let mut body = VecDeque::new();
        for i in 0..len{
            body.push_back(empty);
        }
        Tape{body, len}
    }
}

pub struct Actor {
    pub last_print: u32,
    pub states: u32,
    pub alphabet: Alphabet
}
